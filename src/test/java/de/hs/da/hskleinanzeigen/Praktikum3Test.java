package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_AD;
import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_USER;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;


@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum3Test {

    private static final String CATEGORY_NAME = "Nachhilfe";
    private static final int CATEGORY_ID = 200001;
    private static final String CATEGORY_PAYLOAD = "{\n" +
            "   \"name\":\"" + CATEGORY_NAME + "\"\n" +
            "}\n";
    private static final String CATEGORY_PAYLOAD_NO_VALID_NAMETAG = "{\n" +
            "   \"wrongNameTag\":\"" + CATEGORY_NAME + "\"\n" +
            "}\n";
    private static final int page = 0;
    private static final int size = 5;
    private static final String typeNameValid = "OFFER";
    private static final String USER_EMAIL = "studi@hs-da.de";
    private static final String CREATE_USER_PAYLOAD = "{\n" +
            "   \"email\":\"" + USER_EMAIL + "\",\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";
    private static final String CREATE_USER_PAYLOAD_INCOMPLETE = "{\n" +
            "   \"password\":\"secret\",\n" +
            "   \"firstName\":\"Thomas\",\n" +
            "   \"lastName\":\"Müller\",\n" +
            "   \"phone\":\"069-123456\",\n" +
            "   \"location\":\"Darmstadt\"\n" +
            "}\n";
    private static final String CREATE_ADD_PAYLOAD_INCOMPLETE = "{\n" +
            "    \"type\" : \"OFFER\",\n" +
            "    \"categoryId\" : 200001,\n" +
            "    \"title\":\"Zimmer in 4er WG\",\n" +
            "    \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "    \"price\" : 400,\n" +
            "    \"location\":\"Birkenweg, Darmstadt\"\n" +
            "}";
    private static final String CREATE_ADD_PAYLOAD = "{\n" +
            "    \"type\" : \"OFFER\",\n" +
            "    \"categoryId\": 200001,\n" +
            "    \"title\":\"Zimmer in 4er WG\",\n" +
            "    \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "    \"price\" : 400,\n" +
            "    \"location\":\"Birkenweg, Darmstadt\",\n" +
            "    \"userId\" : 100001\n" +
            "}";
    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
            add("USER");
        }
    };
    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        TestUtil.insertData(connection, expectedTables);
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }


    /**
     * checks if creating a category works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus200() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME, connection);

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("name", equalTo(CATEGORY_NAME))
                .body("keySet()", containsInAnyOrder("id", "name"));

        final String query = "select count(*) from CATEGORY where NAME = \"" + CATEGORY_NAME + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
            Assertions.assertEquals(1, result.getInt(1));
        }
    }

    /**
     * checks if creating an invalid category returns Http.400 (Bad Request)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus400() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME, connection);

        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD_NO_VALID_NAMETAG)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }


    /**
     * checks if creating a category twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_createCategoryStatus409() throws SQLException {
        TestUtil.deleteCategoryInDatabase(CATEGORY_NAME, connection);

        // add category for the first time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add category for the second time
        given()
                .basePath(TestUtil.BASE_PATH_CATEGORY)
                .header("Content-Type", "application/json")
                .body(CATEGORY_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }

    /**
     * checks if the expected tables exists
     * User table is new
     *
     * @throws SQLException if the connection failed or the database is badly configured
     */
    @Test
    void task2_checkUserColumnsExist() throws SQLException {
        boolean hasUser = false;
        List<String> existingTables = new ArrayList<>();

        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            if (expectedTables.contains(rs.getString("TABLE_NAME"))) {
                existingTables.add(rs.getString("TABLE_NAME"));
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());

        ResultSet columns = connection.getMetaData().getColumns(null, null, "AD", null);
        while (columns.next()) {
            if (columns.getString("COLUMN_NAME").equals("USER_ID")) {
                hasUser = true;
                break;
            }
        }
        Assertions.assertTrue(hasUser);
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * EMAIL	Varchar, not null
     * PASSWORD	Varchar, not null
     * FIRST_NAME	Varchar
     * LAST_NAME	Varchar
     * PHONE	Varchar
     * LOCATION	Varchar
     * CREATED	Timestamp, not null
     */
    @Test
    void task2_newUserTableExists() {
        SoftAssertions.assertSoftly(softly -> {
            try {
                softly.assertThat(TestUtil.checkFieldExist(connection, "USER", "ID", TestUtil.FieldType.INTEGER)).as(
                        "Column ID is missing or has invalid data type").isTrue();
                softly.assertThat(TestUtil.checkFieldExist(connection, "USER", "EMAIL", TestUtil.FieldType.VARCHAR)).as(
                        "Column EMAIL is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "USER", "PASSWORD", TestUtil.FieldType.VARCHAR)).as(
                        "Column PASSWORD is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "USER", "FIRST_NAME", TestUtil.FieldType.VARCHAR)).as(
                        "Column FIRST_NAME is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "USER", "LAST_NAME", TestUtil.FieldType.VARCHAR)).as(
                        "Column LAST_NAME is missing or has invalid data type").isTrue();
                softly.assertThat(TestUtil.checkFieldExist(connection, "USER", "PHONE", TestUtil.FieldType.VARCHAR)).as(
                        "Column PHONE is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "USER", "LOCATION", TestUtil.FieldType.VARCHAR)).as(
                        "Column LOCATION is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "USER", "CREATED", TestUtil.FieldType.TIMESTAMP)).as(
                        "Column CREATED is missing or has invalid data type").isTrue();
            } catch (SQLException e) {
                softly.fail("Error checking database: " + e.getMessage());
            }
        });
    }

    /**
     * checks if inserting users works
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_insertUserStatus200() throws SQLException {
        TestUtil.deleteUserInDatabase(USER_EMAIL, connection);

        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("id", notNullValue())
                .body("email", equalTo(USER_EMAIL))
                .body("firstName", equalTo("Thomas"))
                .body("lastName", equalTo("Müller"))
                .body("phone", equalTo("069-123456"))
                .body("location", equalTo("Darmstadt"))
                .body("keySet()", containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));

        final String query = "select count(*) from USER where EMAIL = \"" + USER_EMAIL + "\"";
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next());
            Assertions.assertEquals(1, result.getInt(1));
        }
    }

    /**
     * checks if inserting an invalid user returns Http.400 (Bad Request)
     */
    @Test
    void task3_insertUserStatus400() {
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD_INCOMPLETE)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if inserting a user with the same email twice returns Http.409 (Conflict)
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_insertUserStatus409() throws SQLException {
        // add user for the first time
        TestUtil.deleteUserInDatabase(USER_EMAIL, connection);
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post().then().statusCode(HttpStatus.CREATED.value());

        // add user for the second time
        given()
                .basePath(TestUtil.BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(CREATE_USER_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CONFLICT.value());
    }

    @Test
    void task4_getUser_UserFound() {
        final long userId = 100001;

        given()
                .basePath(BASE_PATH_USER + "/" + userId)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("id", equalTo(100001))
                .body("email", equalTo("some@email.de"))
                .body("firstName", equalTo("Vorname"))
                .body("lastName", equalTo("Nachname"))
                .body("location", equalTo("Standort"))
                .body("phone", equalTo("06254-call-me-maybe"))
                .body("keySet()", containsInAnyOrder("id", "email", "firstName", "lastName", "location", "phone"));
    }

    @Test
    void task4_getUser_UserNotFound() {
        final long userId = -1;

        given()
                .basePath(BASE_PATH_USER + "/" + userId)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks if requesting a User page returns a page and http.200 (OK)
     */
    @Test
    void task5_getUserPageStatus200() {
        given()
                .basePath(BASE_PATH_USER)
                .accept("application/json")
                .queryParams("page", 0, "size", 10)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content[0].id", notNullValue())
                .body("content[0].email", notNullValue())
                .body("content[0].firstName", notNullValue())
                .body("content[0].lastName", notNullValue())
                .body("content[0].phone", notNullValue())
                .body("content[0].location", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", greaterThanOrEqualTo(1))
                .body("totalElements", greaterThanOrEqualTo(1))
                .body("content[0].keySet()",
                      containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));
    }

    @Test
    void task6_getAdPageStatus200() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("page", page, "size", size)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content[0].id", notNullValue())
                .body("content[0].type", notNullValue())
                .body("content[0].title", notNullValue())
                .body("content[0].price", notNullValue())
                .body("content[0].location", notNullValue())
                .body("content[0].description", notNullValue())
                .body("content[0].category", notNullValue())
                .body("content[0].category.id", notNullValue())
                .body("content[0].category.name", notNullValue())
                .body("content[0].user", notNullValue())
                .body("content[0].user.id", notNullValue())
                .body("content[0].user.email", notNullValue())
                .body("content[0].user.firstName", notNullValue())
                .body("content[0].user.lastName", notNullValue())
                .body("content[0].user.phone", notNullValue())
                .body("content[0].user.location", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", greaterThanOrEqualTo(1))
                .body("totalElements", greaterThanOrEqualTo(1))
                .body("content[0].keySet()",
                      containsInAnyOrder("id", "type", "category", "user", "title", "price", "location", "description"))
                .body("content[0].category.keySet()", containsInAnyOrder("id", "name"))
                .body("content[0].user.keySet()",
                      containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));

    }

    @Test
    void task6_getAdPageStatus200WithAllParams() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("type", typeNameValid, "categoryId", CATEGORY_ID, "priceFrom", 0, "priceTo", 600, "page",
                             page, "size", size)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalPages", greaterThanOrEqualTo(1))
                .body("totalElements", greaterThanOrEqualTo(1));
    }

    @Test
    void task6_getAdPageStatus200_NoAdsFoundForGivenPrice() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("type", typeNameValid, "categoryId", CATEGORY_ID, "priceFrom", 100000, "priceTo", 100000,
                             "page", page, "size", size)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalElements", equalTo(0));
    }

    @Test
    void task6_getAdPageStatus200_NoAdsFoundForUnknownCategory() {
        given()
                .basePath(BASE_PATH_AD)
                .accept("application/json")
                .queryParams("type", typeNameValid, "categoryId", 765128, "priceFrom", 0, "priceTo", 600, "page", page,
                             "size", size)
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("content", notNullValue())
                .body("pageable", notNullValue())
                .body("totalElements", equalTo(0));
    }

    /**
     * checks if the requesting an ad matches the new specifications of task6
     */
    @Test
    void task7_getAdResult200() {
        given()
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", 300001)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value())
                .body("id", notNullValue())
                .body("type", equalTo("OFFER"))
                .body("category", notNullValue())
                .body("category.id", equalTo(200001))
                .body("category.name", equalTo("Name"))
                .body("user", notNullValue())
                .body("user.id", equalTo(100001))
                .body("user.email", equalTo("some@email.de"))
                .body("user.firstName", equalTo("Vorname"))
                .body("user.lastName", equalTo("Nachname"))
                .body("user.phone", equalTo("06254-call-me-maybe"))
                .body("user.location", equalTo("Standort"))
                .body("title", equalTo("Titel"))
                .body("description", equalTo("Beschreibung"))
                .body("price", equalTo(42))
                .body("location", equalTo("Standort"))
                .body("keySet()",
                      containsInAnyOrder("id", "type", "category", "user", "title", "description", "price", "location"))
                .body("category.keySet()", containsInAnyOrder("id", "name"))
                .body("user.keySet()", containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));
    }

    /**
     * checks if requesting an illegal id returns Http.Bad Request
     */
    @Test
    void task7_getAdResult400() {
        given()
                .accept("application/json")
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", -1)
                .when().get("{id}")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks the rest endpoint for creating an advertisement
     */
    @Test
    void task8_createAdResult201() {
        given()
                .basePath(BASE_PATH_AD)
                .header("Content-Type", "application/json")
                .body(CREATE_ADD_PAYLOAD)
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .contentType("application/json")
                .body("id", notNullValue())
                .body("type", equalTo("OFFER"))
                .body("category", notNullValue())
                .body("category.id", equalTo(200001))
                .body("category.name", equalTo("Name"))
                .body("user", notNullValue())
                .body("user.id", equalTo(100001))
                .body("user.email", equalTo("some@email.de"))
                .body("user.firstName", equalTo("Vorname"))
                .body("user.lastName", equalTo("Nachname"))
                .body("user.phone", equalTo("06254-call-me-maybe"))
                .body("user.location", equalTo("Standort"))
                .body("title", equalTo("Zimmer in 4er WG"))
                .body("description", equalTo("Wohnheim direkt neben der HS"))
                .body("price", equalTo(400))
                .body("location", equalTo("Birkenweg, Darmstadt"))
                .body("keySet()",
                      containsInAnyOrder("id", "type", "category", "user", "title", "description", "price", "location"))
                .body("category.keySet()", containsInAnyOrder("id", "name"))
                .body("user.keySet()", containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));
    }

    /**
     * checks if creating an incomplete advertisement returns HTTP.400 (Bad Request)
     */
    @Test
    void task8_createAdResult400() {
        given()
                .basePath(BASE_PATH_AD)
                .header("Content-Type", "application/json")
                .body(CREATE_ADD_PAYLOAD_INCOMPLETE)
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }
}
