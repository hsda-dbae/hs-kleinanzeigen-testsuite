package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_AD;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum2Test {

    private static final String payload = "{\n" +
            "   \"type\":\"OFFER\",\n" +
            "   \"categoryId\" : 200001,\n" +
            "   \"title\":\"Zimmer in 4er WG\",\n" +
            "   \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "   \"price\":400,\n" +
            "   \"location\":\"Birkenweg, Darmstadt\"\n" +
            "}\n";
    private static final String payload_withUser = "{\n" +
            "    \"type\" : \"OFFER\",\n" +
            "    \"categoryId\" : 200001,\n" +
            "    \"title\":\"Zimmer in 4er WG\",\n" +
            "    \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "    \"price\" : 400,\n" +
            "    \"location\":\"Birkenweg, Darmstadt\",\n" +
            "    \"userId\": 100001\n" +
            "}";
    private static final String payload_incomplete = "{\n" +
            "   \"categoryId\" : 200001,\n" +
            "   \"title\":\"Zimmer in 4er WG\",\n" +
            "   \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "   \"price\":400,\n" +
            "   \"location\":\"Birkenweg, Darmstadt\"\n" +
            "}\n";
    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
        }
    };


    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        if (TestUtil.doesColumnUserExist(connection)) {
            expectedTables.add("USER");
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        TestUtil.insertData(connection, expectedTables);
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * checks if the required tables (defined in expectedTables) exist in the Database
     *
     * @throws SQLException if the connection failed
     */
    @Test
    void task1_checkRequiredTablesExist() throws SQLException {
        ArrayList<String> existingTables = TestUtil.checkExistingTables(connection, expectedTables);

        Assertions.assertEquals(expectedTables.size(), existingTables.size());
    }

    /**
     * expected Table layout:
     * ID	Integer, Primary Key, auto increment
     * TYPE	Enumeration (OFFER, REQUEST), not null
     * CATEGORY_ID	Integer, Foreign Key auf Tabelle CATEGORY, not null
     * TITLE	Varchar, not null
     * DESCRIPTION	Varchar, not null
     * PRICE	Integer
     * LOCATION	Varchar
     * CREATED	Timestamp, not null
     */
    @Test
    void task1_checkAdColumnsExist() {
        SoftAssertions.assertSoftly(softly -> {
            try {
                softly.assertThat(TestUtil.checkFieldExist(connection, "AD", "ID", TestUtil.FieldType.INTEGER)).as(
                        "Column ID is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "AD", "TYPE", TestUtil.FieldType.ENUMERATION)).as(
                        "Column TYPE is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "AD", "CATEGORY_ID", TestUtil.FieldType.INTEGER)).as(
                        "Column CATEGORY_ID is missing or has invalid data type").isTrue();
                softly.assertThat(TestUtil.checkFieldExist(connection, "AD", "TITLE", TestUtil.FieldType.VARCHAR)).as(
                        "Column TITLE is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "AD", "DESCRIPTION", TestUtil.FieldType.VARCHAR)).as(
                        "Column DESCRIPTION is missing or has invalid data type").isTrue();
                softly.assertThat(TestUtil.checkFieldExist(connection, "AD", "PRICE", TestUtil.FieldType.INTEGER)).as(
                        "Column PRICE is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "AD", "LOCATION", TestUtil.FieldType.VARCHAR)).as(
                        "Column LOCATION is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "AD", "CREATED", TestUtil.FieldType.TIMESTAMP)).as(
                        "Column CREATED is missing or has invalid data type").isTrue();
            } catch (SQLException e) {
                softly.fail("Error checking database: " + e.getMessage());
            }
        });
    }

    /**
     * expected Table layout:
     * ID	Integer, Primary Key, auto increment
     * NAME	Varchar
     */
    @Test
    void task1_checkCategoryColumnsExist() {
        SoftAssertions.assertSoftly(softly -> {
            try {
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "CATEGORY", "ID", TestUtil.FieldType.INTEGER)).as(
                        "Column ID is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "CATEGORY", "NAME", TestUtil.FieldType.VARCHAR)).as(
                        "Column NAME is missing or has invalid data type").isTrue();
            } catch (SQLException e) {
                softly.fail("Error checking database: " + e.getMessage());
            }
        });
    }

    /**
     * checks the rest endpoint for creating an advertisement
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_createAdResult201() throws SQLException {
        final String requestPayload;
        if (TestUtil.doesColumnUserExist(connection)) {
            requestPayload = payload_withUser;
            given()
                    .basePath(BASE_PATH_AD)
                    .header("Content-Type", "application/json")
                    .body(requestPayload)
                    .accept("application/json")
                    .when().post()
                    .then().statusCode(HttpStatus.CREATED.value())
                    .contentType("application/json")
                    .body("id", notNullValue())
                    .body("type", equalTo("OFFER"))
                    .body("category", notNullValue())
                    .body("category.id", equalTo(200001))
                    .body("category.name", equalTo("Name"))
                    .body("title", equalTo("Zimmer in 4er WG"))
                    .body("description", equalTo("Wohnheim direkt neben der HS"))
                    .body("price", equalTo(400))
                    .body("location", equalTo("Birkenweg, Darmstadt"))
                    .body("keySet()",
                          containsInAnyOrder("id", "type", "category", "title", "description", "price", "location",
                                             "user"))
                    .body("category.keySet()", containsInAnyOrder("id", "name"));
        } else {
            requestPayload = payload;
            given()
                    .basePath(BASE_PATH_AD)
                    .header("Content-Type", "application/json")
                    .body(requestPayload)
                    .accept("application/json")
                    .when().post()
                    .then().statusCode(HttpStatus.CREATED.value())
                    .contentType("application/json")
                    .body("id", notNullValue())
                    .body("type", equalTo("OFFER"))
                    .body("category", notNullValue())
                    .body("category.id", equalTo(200001))
                    .body("category.name", equalTo("Name"))
                    .body("title", equalTo("Zimmer in 4er WG"))
                    .body("description", equalTo("Wohnheim direkt neben der HS"))
                    .body("price", equalTo(400))
                    .body("location", equalTo("Birkenweg, Darmstadt"))
                    .body("keySet()",
                          containsInAnyOrder("id", "type", "category", "title", "description", "price", "location"))
                    .body("category.keySet()", containsInAnyOrder("id", "name"));
        }
    }

    /**
     * checks if creating an incomplete advertisement returns HTTP.400 (Bad Request)
     */
    @Test
    void task3_createAdResult400() {
        given()
                .basePath(BASE_PATH_AD)
                .header("Content-Type", "application/json")
                .body(payload_incomplete)
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    /**
     * checks if the rest-endpoint for getting an existing advertisement works
     */
    @Test
    void task4_getAdResult200() throws SQLException {
        if (TestUtil.doesColumnUserExist(connection)) {
            given()
                    .basePath(BASE_PATH_AD)
                    .pathParam("id", 300001)
                    .accept("application/json")
                    .when().get("{id}")
                    .then().statusCode(HttpStatus.OK.value())
                    .body("id", notNullValue())
                    .body("type", equalTo("OFFER"))
                    .body("category", notNullValue())
                    .body("category.id", equalTo(200001))
                    .body("category.name", equalTo("Name"))
                    .body("title", equalTo("Titel"))
                    .body("description", equalTo("Beschreibung"))
                    .body("price", equalTo(42))
                    .body("location", equalTo("Standort"))
                    .body("keySet()",
                          containsInAnyOrder("id", "type", "category", "user", "title", "description", "price",
                                             "location"))
                    .body("category.keySet()", containsInAnyOrder("id", "name"));
        } else {
            given()
                    .basePath(BASE_PATH_AD)
                    .pathParam("id", 300001)
                    .accept("application/json")
                    .when().get("{id}")
                    .then().statusCode(HttpStatus.OK.value())
                    .body("id", notNullValue())
                    .body("type", equalTo("OFFER"))
                    .body("category", notNullValue())
                    .body("category.id", equalTo(200001))
                    .body("category.name", equalTo("Name"))
                    .body("title", equalTo("Titel"))
                    .body("description", equalTo("Beschreibung"))
                    .body("price", equalTo(42))
                    .body("location", equalTo("Standort"))
                    .body("keySet()",
                          containsInAnyOrder("id", "type", "category", "title", "description", "price", "location"))
                    .body("category.keySet()", containsInAnyOrder("id", "name"));
        }
    }

    /**
     * checks if the rest-endpoint for querying a not existing advertisement returns Http.404 (Not found)
     */
    @Test
    void task4_getAdResult404() {
        given()
                .accept("application/json")
                .basePath(BASE_PATH_AD)
                .pathParam("id", -1)
                .when().get("{id}")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks allowed endpoints for user WITHOUT authentication
     */
    @Test
    void task5_allowedEndpointsNoCredentials() {
        TestUtil.setNoAuthentication();

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * checks forbidden endpoints for user WITHOUT authentication
     */
    @Test
    void task5_forbiddenEndpointsNoCredentials() {
        TestUtil.setNoAuthentication();

        given()
                .basePath(BASE_PATH_AD)
                .pathParam("id", 300001)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    /**
     * checks forbidden endpoints for user with INVALID authentication
     */
    @Test
    void task5_forbiddenEndpointsInvalidCredentials() {
        TestUtil.setAuthentication("user", "asdadfjhglkdfgfg");

        given()
                .basePath(BASE_PATH_AD)
                .pathParam("id", 300001)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    /**
     * checks allowed enpoints for user with USER authentication
     */
    @Test
    void task5_allowedEndpointsUserCredentials() {
        TestUtil.setAuthenticationForUser();

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());

        given()
                .basePath(BASE_PATH_AD)
                .pathParam("id", 300001)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * checks forbidden enpoints for user with USER authentication
     */
    @Test
    void task5_forbiddenEndpointsUserCredentials() {
        TestUtil.setAuthenticationForUser();

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.FORBIDDEN.value());
    }

    /**
     * checks allowed enpoints for user with ADMIN authentication
     */
    @Test
    void task5_allowedEndpointsAdminCredentials() {
        TestUtil.setAuthentication("admin", "admin");

        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/health")//
                .then().statusCode(HttpStatus.OK.value());

        given().when().get("/hs-kleinanzeigen/actuator/metrics")//
                .then().statusCode(HttpStatus.OK.value());

        given()
                .basePath(BASE_PATH_AD)
                .pathParam("id", 300001)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value());
    }
}
