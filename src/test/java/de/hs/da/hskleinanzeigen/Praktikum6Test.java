package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_USER;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum6Test {

    private static final Set<String> expectedTables = Collections.singleton("USER");

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        TestUtil.insertData(connection, expectedTables);
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * Test only ensures that Redis is started at the defined host and port.
     * It does not ensure that the application is configured to use Redis correctly.
     */
    @Test
    void task1_checkRedisUpAndRunning() {
        RedisConnection connection = TestUtil.getRedisConnection();
        assertThat(connection).isNotNull();
        assertThat(connection.keys("*".getBytes())).hasSizeGreaterThanOrEqualTo(0);
    }

    @Test
    void task2_createdUserIsCachedInRedis() {
        invalidateRedisCache();
        final User user = new User();

        final long userId = given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .extract().jsonPath().getLong("id");


        boolean userFound = findUserInCache(userId);
        assertThat(userFound).describedAs("No cache found for id " + userId).isTrue();
    }


    @Test
    void task2_getUserIsCachedInRedis() {
        invalidateRedisCache();
        final long userId = 100001;

        given()
                .basePath(BASE_PATH_USER + "/" + userId)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.OK.value());

        boolean userFound = findUserInCache(userId);
        assertThat(userFound).describedAs("No cache found for id " + userId).isTrue();
    }


    private void invalidateRedisCache() {
        RedisConnection redisConnection = TestUtil.getRedisConnection();
        Objects.requireNonNull(redisConnection.keys("*".getBytes())).forEach(redisConnection::del);
    }

    private Set<String> getAllRedisKeys() {
        Set<byte[]> keys = TestUtil.getRedisConnection().keys("*".getBytes());
        return keys.stream().map(k -> new String(k, 0, k.length)).collect(Collectors.toSet());
    }

    private boolean findUserInCache(long userId) {
        boolean userFound = false;
        for (String key : getAllRedisKeys()) {
            if (key.endsWith(String.valueOf(userId))) {
                userFound = true;
                break;
            }
        }
        return userFound;
    }

    class User {
        String email = UUID.randomUUID().toString() + "@mynewdomain.de";
        String password = "secret";
        String firstName = "my first name";
        String lastName = "my second name";
        String phone = "0176822222222";
        String location = "Darmstadt";

        String toJson() {
            String json = "{\n";
            if (email != null) {
                json += "    \"email\": \"" + email + "\",\n";

            }
            if (password != null) {
                json += "    \"password\": \"" + password + "\",\n";

            }

            if (firstName != null) {


                json += "    \"firstName\": \"" + firstName + "\",\n";
            }

            if (lastName != null) {
                json += "    \"lastName\": \"" + lastName + "\",\n";

            }

            json += "    \"phone\": \"" + phone + "\",\n" +
                    "    \"location\": \"" + location + "\"\n" +
                    "}";
            return json;
        }
    }
}
