package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static de.hs.da.hskleinanzeigen.TestUtil.BASE_PATH_USER;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum4Test {

    private static final String NOTEPAD_AD_ID = "300001";

    private static final String NOTEPAD_PAYLOAD = "{\n" +
            "        \"advertisementId\": " + NOTEPAD_AD_ID + ",\n" +
            "        \"note\": \"Zimmer direkt bei der HS\"\n" +
            "    }\n";

    private static final String NOTEPAD_PAYLOAD_NOTE_CHANGED = "{\n" +
            "        \"advertisementId\": " + NOTEPAD_AD_ID + ",\n" +
            "        \"note\": \"Beste WG ever!\"\n" +
            "    }\n";

    private static final String NOTEPAD_PAYLOAD_INCOMPLETE = "{\n" +
            "   \"note\":\"Zimmer direkt bei der HS\"\n" +
            "}\n";

    private static final Set<String> expectedTables = new HashSet<>(Arrays.asList("AD", "CATEGORY", "USER", "NOTEPAD"));

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private boolean initialized = false;

    @BeforeEach
    void setUp() throws Exception {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        if (!initialized) {
            TestUtil.insertData(connection, expectedTables);
            initialized = true;
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Test
    void task5_checkSwaggerUIAvailable() {
        given().when().get("/hs-kleinanzeigen/swagger-ui/index.html")//
                .then().statusCode(HttpStatus.OK.value());
    }

    /**
     * @noinspection unchecked
     */
    @Test
    void task5_checkApiDocsAdvertisementsEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        // supported frameworks: springfox & springdoc-openapi-ui
        final String expectedPath = paths.containsKey(
                "/hs-kleinanzeigen/api/advertisements/{id}") ? "/hs-kleinanzeigen/api/advertisements/{id}" : "/api/advertisements/{id}";
        assertThat(paths).containsKey(expectedPath);
        assertThat(paths.get(expectedPath)).containsKey("get");
        assertThat(((List) paths.get(expectedPath).get("get").get("parameters"))).hasSize(1);
        assertThat(((Map) paths.get(expectedPath).get("get").get("responses"))).hasSizeGreaterThanOrEqualTo(2);
        assertThat(((Map) paths.get(expectedPath).get("get").get("responses"))).containsKeys("200", "404");
    }

    /**
     * @noinspection unchecked
     */
    @Test
    void task5_checkApiDocsUserEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        // supported frameworks: springfox & springdoc-openapi-ui
        final String expectedPath = paths.containsKey(
                "/hs-kleinanzeigen/api/users") ? "/hs-kleinanzeigen/api/users" : "/api/users";
        assertThat(paths).containsKey(expectedPath);
        assertThat(paths.get(expectedPath)).containsKey("get");
        assertThat(((List) paths.get(expectedPath).get("get").get("parameters"))).hasSize(2);
        assertThat(((Map) paths.get(expectedPath).get("get").get("responses"))).hasSizeGreaterThanOrEqualTo(2);
        assertThat(((Map) paths.get(expectedPath).get("get").get("responses"))).containsKeys("200", "400");
    }

    /**
     * @noinspection unchecked
     */
    @Test
    void task5_checkApiDocsCategoriesEndpoint() {
        Map<String, Map<String, Map<String, Object>>> paths = readApiDocsEndpoints();

        // supported frameworks: springfox & springdoc-openapi-ui
        final String expectedPath = paths.containsKey(
                "/hs-kleinanzeigen/api/categories") ? "/hs-kleinanzeigen/api/categories" : "/api/categories";
        assertThat(paths).containsKey(expectedPath);
        assertThat(paths.get(expectedPath)).containsKey("post");
        assertThat(((List) paths.get(expectedPath).get("post").get("parameters"))).isNullOrEmpty();
        assertThat(((Map) paths.get(expectedPath).get("post").get("responses"))).hasSizeGreaterThanOrEqualTo(3);
        assertThat(((Map) paths.get(expectedPath).get("post").get("responses"))).containsKeys("201", "400", "409");
    }

    private Map<String, Map<String, Map<String, Object>>> readApiDocsEndpoints() {
        // Map<PATH, Map<OPERATION, Map<[tags|summary|operationId|parameters|responses], Object>>>
        return given().when().get("/hs-kleinanzeigen/v3/api-docs")//
                .then().statusCode(HttpStatus.OK.value())
                .extract().jsonPath().getMap("paths");
    }

    /**
     * checks if the endpoint for creating a user after adding the checks for email and name validation still returns http.201 (created)
     * and the returned user doesn't contain the password.
     */
    @Test
    void task6_createUserStatus201() {
        User user = new User();

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.CREATED.value())
                .body("$", hasKey("id"))
                .body("$", hasKey("email"))
                .body("$", hasKey("firstName"))
                .body("$", hasKey("lastName"))
                .body("$", hasKey("phone"))
                .body("$", hasKey("location"))
                .body("keySet()", containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));

    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserBadEmailStatus400() {
        User user = new User();
        user.email = "invalidEmail";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoEmailStatus400() {
        User user = new User();
        user.email = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserPasswordToShortStatus400() {
        User user = new User();
        user.password = "kurz";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoPasswordStatus400() {
        User user = new User();
        user.password = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoFirstNameStatus400() {
        User user = new User();
        user.firstName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserFirstNameTooLongStatus400() {
        User user = new User();
        user.firstName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserNoLastNameStatus400() {
        User user = new User();
        user.lastName = null;

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if the enpoint for creating an invalid user returns http.400 (bad Request)
     */
    @Test
    void task6_createUserLastNameTooLongStatus400() {
        User user = new User();
        user.lastName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. " +
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. " +
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";

        given()
                .basePath(BASE_PATH_USER)
                .header("Content-Type", "application/json")
                .body(user.toJson())
                .accept("application/json")
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    void task7_liquibaseTablesExists() throws SQLException {
        Set<String> existingTables = new HashSet<>();
        DatabaseMetaData md = connection.getMetaData();
        try (ResultSet rs = md.getTables(null, null, "%", null)) {
            while (rs.next()) {
                existingTables.add(rs.getString("TABLE_NAME").toUpperCase());
            }
        }

        assertThat(existingTables).contains("DATABASECHANGELOG").withFailMessage(
                "Liquibase meta data table 'DATABASECHANGELOG' not found in database");
        assertThat(existingTables).contains("DATABASECHANGELOGLOCK").withFailMessage(
                "Liquibase meta data table 'DATABASECHANGELOGLOCK' not found in database");

        assertThat(existingTables).contains("CATEGORY").withFailMessage("table 'CATEGORY' not found in database");
        assertThat(existingTables).contains("AD").withFailMessage("table 'AD' not found in database");
    }

    @Test
    void task8_checkNotepadTableExists() throws SQLException {
        Set<String> existingTables = new HashSet<>();
        DatabaseMetaData md = connection.getMetaData();
        try (ResultSet rs = md.getTables(null, null, "%", null)) {
            while (rs.next()) {
                String tableName = rs.getString("TABLE_NAME");
                if (expectedTables.contains(tableName)) {
                    existingTables.add(tableName.toUpperCase());
                }
            }
        }

        Assertions.assertEquals(expectedTables.size(), existingTables.size());
    }

    /**
     * expected table layout:
     * ID	Integer, Primary Key, auto increment
     * USER_ID	Integer, not null, Foreign Key auf USER.ID
     * AD_ID	Integer, not null, Foreign Key auf AD.ID
     * NOTE	Varchar
     * CREATED	Timestamp, not null
     */
    @Test
    void task8_checkNotepadColumnsExist() {
        SoftAssertions.assertSoftly(softly -> {
            try {
                softly.assertThat(TestUtil.checkFieldExist(connection, "NOTEPAD", "ID", TestUtil.FieldType.INTEGER)).as(
                        "Column ID is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "NOTEPAD", "USER_ID", TestUtil.FieldType.INTEGER)).as(
                        "Column USER_ID is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "NOTEPAD", "AD_ID", TestUtil.FieldType.INTEGER)).as(
                        "Column AD_ID is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "NOTEPAD", "NOTE", TestUtil.FieldType.VARCHAR)).as(
                        "Column NOTE is missing or has invalid data type").isTrue();
                softly.assertThat(
                        TestUtil.checkFieldExist(connection, "NOTEPAD", "CREATED", TestUtil.FieldType.TIMESTAMP)).as(
                        "Column CREATED is missing or has invalid data type").isTrue();
            } catch (SQLException e) {
                softly.fail("Error checking database: " + e.getMessage());
            }
        });
    }

    /**
     * checks if creating a notepad returns http.200 (ok)
     * and the notepad is persisted in the database
     */
    @Test
    void task9_createNotepadStatus200() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            final String deleteNotepadStatement = "DELETE FROM NOTEPAD\n" +
                    "WHERE AD_ID = " + NOTEPAD_AD_ID;
            statement.executeUpdate(deleteNotepadStatement);
        }

        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 100001)
                .body(NOTEPAD_PAYLOAD)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.OK.value());

        try (Statement statement = connection.createStatement()) {
            final String query = "select * from NOTEPAD\n" +
                    "where AD_ID = \"" + NOTEPAD_AD_ID + "\" and USER_ID = 100001";
            ResultSet result = statement.executeQuery(query);
            Assertions.assertTrue(result.next(),
                                  "No notepad found in Database for the User ID / 1 and Notepad ID / " + NOTEPAD_AD_ID + ".");
        }
    }

    /**
     * checks if posting an invalid notepad returns http.400 (Bad REQUEST)
     */
    @Test
    void task9_createNotepadStatus400_UserUnknwon() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 3462384)
                .body(NOTEPAD_PAYLOAD_INCOMPLETE)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * checks if updating a notepad returns HTTP 200
     */
    @Test
    void task9_updateNotepad200() {
        // add (or update) notepad for the first time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 100001)
                .body(NOTEPAD_PAYLOAD)
                .accept("application/json")
                .when().put().then().statusCode(HttpStatus.OK.value());

        // add notepad for the second time
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 100001)
                .body(NOTEPAD_PAYLOAD_NOTE_CHANGED)
                .accept("application/json")
                .when().put()
                .then().statusCode(HttpStatus.OK.value())
                .body("keySet()", containsInAnyOrder("id"));
    }

    /**
     * checks if http get on the notepad of a user returns http.200 (OK)
     */
    @Test
    void task10_getNotepadStatus200() {
        given().log().all()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", 100001)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.OK.value())
                .body("size()", greaterThanOrEqualTo(1))
                .body("[0].id", notNullValue())
                .body("[0].advertisement", notNullValue())
                .body("[0].advertisement.id", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.type", either(equalTo("OFFER")).or(equalTo("REQUEST")))
                .body("[0].advertisement.category", notNullValue())
                .body("[0].advertisement.category.id", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.category.name", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.user", notNullValue())
                .body("[0].advertisement.user.id", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.user.email", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.user.firstName", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.user.lastName", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.user.phone", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.user.location", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.title", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.description", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].advertisement.price", allOf(instanceOf(Integer.class), notNullValue()))
                .body("[0].advertisement.location", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].note", allOf(instanceOf(String.class), notNullValue()))
                .body("[0].keySet()", containsInAnyOrder("id", "advertisement", "note"))
                .body("[0].advertisement.keySet()",
                      containsInAnyOrder("id", "type", "category", "description", "user", "title", "price", "location"))
                .body("[0].advertisement.category.keySet()", containsInAnyOrder("id", "name"))
                .body("[0].advertisement.user.keySet()",
                      containsInAnyOrder("id", "email", "firstName", "lastName", "phone", "location"));

    }

    /**
     * checks if requesting the notepad of a not existing user returns http.04 (Not found)
     */
    @Test
    void task10_getNotepadStatus404() {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .pathParam("userId", -1)
                .accept("application/json")
                .when().get()
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    /**
     * checks if deleting a notepad returns http.204 (no content)
     * and the notepad no longer exists in database
     */
    @Test
    void task11_deleteNotepadStatus204() throws SQLException {
        given()
                .basePath(TestUtil.BASE_PATH_NOTEPAD)
                .header("Content-Type", "application/json")
                .pathParam("userId", 100001)
                .queryParam("advertisementId", NOTEPAD_AD_ID)
                .when().delete()
                .then().statusCode(HttpStatus.NO_CONTENT.value());

        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT COUNT(ID) AS ANZAHL FROM NOTEPAD WHERE USER_ID = 100001 AND AD_ID = " + NOTEPAD_AD_ID);
            resultSet.next();
            int numberOfNotepads = resultSet.getInt("ANZAHL");
            Assertions.assertEquals(0, numberOfNotepads,
                                    "The notepad for user ID /1 and AD ID /" + NOTEPAD_AD_ID + " still exists.");
        }
    }

    static class User {
        String email = UUID.randomUUID() + "@mynewdomain.de";
        String password = "secret";
        String firstName = "my first name";
        String lastName = "my second name";
        String phone = "0176822222222";
        String location = "Darmstadt";

        String toJson() {
            String json = "{\n";
            if (email != null) {
                json += "    \"email\": \"" + email + "\",\n";

            }
            if (password != null) {
                json += "    \"password\": \"" + password + "\",\n";

            }

            if (firstName != null) {


                json += "    \"firstName\": \"" + firstName + "\",\n";
            }

            if (lastName != null) {
                json += "    \"lastName\": \"" + lastName + "\",\n";

            }

            json += "    \"phone\": \"" + phone + "\",\n" +
                    "    \"location\": \"" + location + "\"\n" +
                    "}";
            return json;
        }
    }
}
