package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class TestUtil {

    static final String HOST = "http://localhost";
    static final String BASE_PATH_CATEGORY = "/hs-kleinanzeigen/api/categories";
    static final String BASE_PATH_USER = "/hs-kleinanzeigen/api/users";
    static final String BASE_PATH_NOTEPAD = "/hs-kleinanzeigen/api/users/{userId}/notepad";
    static final String BASE_PATH_AD = "/hs-kleinanzeigen/api/advertisements";
    static final int PORT = 8081;

    /**
     * This function checks if the spring-boot settings are enabled.
     *
     * @return True if basic-auth is required
     */
    public static boolean isSecurityEnabled() throws IOException {
        URL url = new URL(HOST + ":" + PORT + BASE_PATH_AD + "1");
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            return con.getResponseCode() == 403 || con.getResponseCode() == 401;
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }

    public static RedisConnection getRedisConnection() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.getStandaloneConfiguration().setHostName("localhost");
        jedisConnectionFactory.getStandaloneConfiguration().setPort(6379);
        jedisConnectionFactory.afterPropertiesSet();
        return jedisConnectionFactory.getConnection();
    }

    /**
     * This function sets the authentication to the User settings
     */
    public static void setAuthenticationForUser() {
        setAuthentication("user", "user");
    }

    /**
     * This function sets the authentication to not authenticated
     */
    public static void setNoAuthentication() {
        RestAssured.authentication = RestAssured.DEFAULT_AUTH;
    }

    /**
     * This sets the authentication to the provided basic-auth credentials
     *
     * @param userName The username to use
     * @param password The password to use
     */
    public static void setAuthentication(String userName, String password) {
        PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
        authScheme.setUserName(userName);
        authScheme.setPassword(password);
        RestAssured.authentication = authScheme;
    }

    /**
     * This function returns a list of tables existing in the Database.
     *
     * @param connection     The SQL-Connection to use
     * @param expectedTables The list of tables to check for
     * @return The list of Tables actually existing
     */
    public static ArrayList<String> checkExistingTables(Connection connection,
                                                        ArrayList<String> expectedTables) throws SQLException {
        ArrayList<String> existingTables = new ArrayList<>();

        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            //System.out.println(rs.getString("TABLE_NAME"));
            if (expectedTables.contains(rs.getString("TABLE_NAME"))) {
                existingTables.add(rs.getString("TABLE_NAME"));
            }
        }

        return existingTables;
    }

    /**
     * This function checks if the database has a User Table.
     *
     * @param connection The connection to use
     * @return True if the UserColumnExists
     */
    public static boolean doesColumnUserExist(Connection connection) throws SQLException {
        boolean hasUser = false;
        ResultSet columns = connection.getMetaData().getColumns(null, null, "AD", null);
        while (columns.next()) {
            if (columns.getString("COLUMN_NAME").equals("USER_ID")) {
                hasUser = true;
            }
        }
        return hasUser;
    }

    public static boolean checkFieldExist(Connection connection, String tableName, String fieldName,
                                          FieldType fieldType) throws SQLException {
        ResultSet column = connection.getMetaData().getColumns("KLEINANZEIGEN", null, tableName, fieldName);

        for (String type : fieldType.values) {
            if (checkFieldExist(column, fieldName, type)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkFieldExist(ResultSet column, String fieldName, String fieldType) throws SQLException {
        String actualName = "";
        String actualType = "";
        while (column.next()) {
            actualName = column.getString("COLUMN_NAME");
            actualType = column.getString("DATA_TYPE");
        }

        return actualName.equals(fieldName) && actualType.equals(fieldType);
    }

    /**
     * This Function is called by every TestClass to set up test-data if the table exists.
     *
     * @param connection     The SQL connection to use
     * @param expectedTables The SQL tables that should exist
     */
    public static void insertData(Connection connection, Collection<String> expectedTables) throws SQLException {
        boolean userTableExists = false;
        final boolean columnUserExist = doesColumnUserExist(connection);
        final List<String> existingTables = new ArrayList<>();
        DatabaseMetaData md = connection.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            final String tableName = rs.getString("TABLE_NAME");
            if (expectedTables.contains(tableName)) {
                existingTables.add(tableName);
            }
        }

        cleanupTables(connection, columnUserExist);

        try (Statement statement = connection.createStatement()) {
            if (existingTables.contains("USER")) {
                userTableExists = true;
                statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`USER`\n" +
                                                "(`ID`,\n" +
                                                "`EMAIL`,\n" +
                                                "`FIRST_NAME`,\n" +
                                                "`LAST_NAME`,\n" +
                                                "`LOCATION`,\n" +
                                                "`PASSWORD`,\n" +
                                                "`PHONE`,\n" +
                                                "`CREATED`)\n" +
                                                "VALUES\n" +
                                                "(100001,\n" +
                                                "\"some@email.de\",\n" +
                                                "\"Vorname\",\n" +
                                                "\"Nachname\",\n" +
                                                "\"Standort\",\n" +
                                                "\"pass123supi\",\n" +
                                                "\"06254-call-me-maybe\",\n" +
                                                "NOW());");
            }


            if (existingTables.contains("CATEGORY")) {
                statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`CATEGORY`\n" +
                                                "(`ID`,\n" +
                                                "`NAME`)\n" +
                                                "VALUES\n" +
                                                "(200001,\n" +
                                                "\"Name\");\n");

                statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`CATEGORY`\n" +
                                                "(`ID`,\n" +
                                                "`NAME`)\n" +
                                                "VALUES\n" +
                                                "(200002,\n" +
                                                "\"Name\");\n");

            }

            if (existingTables.contains("AD")) {
                if (userTableExists) {
                    statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`AD`\n" +
                                                    "(`ID`,\n" +
                                                    "`TYPE`,\n" +
                                                    "`CATEGORY_ID`,\n" +
                                                    "`USER_ID`,\n" +
                                                    "`TITLE`,\n" +
                                                    "`DESCRIPTION`,\n" +
                                                    "`PRICE`,\n" +
                                                    "`LOCATION`,\n" +
                                                    "`CREATED`)\n" +
                                                    "VALUES\n" +
                                                    "(300001,\n" +
                                                    "\"OFFER\",\n" +
                                                    "200001,\n" +
                                                    "100001,\n" +
                                                    "\"Titel\",\n" +
                                                    "\"Beschreibung\",\n" +
                                                    "42,\n" +
                                                    "\"Standort\",\n" +
                                                    "NOW());");

                    statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`AD`\n" +
                                                    "(`ID`,\n" +
                                                    "`TYPE`,\n" +
                                                    "`CATEGORY_ID`,\n" +
                                                    "`USER_ID`,\n" +
                                                    "`TITLE`,\n" +
                                                    "`DESCRIPTION`,\n" +
                                                    "`PRICE`,\n" +
                                                    "`LOCATION`,\n" +
                                                    "`CREATED`)\n" +
                                                    "VALUES\n" +
                                                    "(300002,\n" +
                                                    "\"REQUEST\",\n" +
                                                    "200001,\n" +
                                                    "100001,\n" +
                                                    "\"Titel\",\n" +
                                                    "\"Beschreibung\",\n" +
                                                    "42,\n" +
                                                    "\"Standort\",\n" +
                                                    "NOW());");
                } else {
                    statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`AD`\n" +
                                                    "(`ID`,\n" +
                                                    "`TYPE`,\n" +
                                                    "`CATEGORY_ID`,\n" +
                                                    "`TITLE`,\n" +
                                                    "`DESCRIPTION`,\n" +
                                                    "`PRICE`,\n" +
                                                    "`LOCATION`,\n" +
                                                    "`CREATED`)\n" +
                                                    "VALUES\n" +
                                                    "(300001,\n" +
                                                    "\"OFFER\",\n" +
                                                    "200001,\n" +
                                                    "\"Titel\",\n" +
                                                    "\"Beschreibung\",\n" +
                                                    "42,\n" +
                                                    "\"Standort\",\n" +
                                                    "NOW());\n");

                    statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`AD`\n" +
                                                    "(`ID`,\n" +
                                                    "`TYPE`,\n" +
                                                    "`CATEGORY_ID`,\n" +
                                                    "`TITLE`,\n" +
                                                    "`DESCRIPTION`,\n" +
                                                    "`PRICE`,\n" +
                                                    "`LOCATION`,\n" +
                                                    "`CREATED`)\n" +
                                                    "VALUES\n" +
                                                    "(300002,\n" +
                                                    "\"REQUEST\",\n" +
                                                    "200001,\n" +
                                                    "\"Titel\",\n" +
                                                    "\"Beschreibung\",\n" +
                                                    "42,\n" +
                                                    "\"Standort\",\n" +
                                                    "NOW());\n");
                }
            }

            if (existingTables.contains("NOTEPAD")) {
                statement.executeUpdate("INSERT INTO `KLEINANZEIGEN`.`NOTEPAD`\n" +
                                                "(`ID`,\n" +
                                                "`USER_ID`,\n" +
                                                "`AD_ID`,\n" +
                                                "`NOTE`,\n" +
                                                "`CREATED`)\n" +
                                                "VALUES\n" +
                                                "(400001,\n" +
                                                "100001,\n" +
                                                "300001,\n" +
                                                "\"Notiz\",\n" +
                                                "NOW());\n");

            }
        }
    }

    /**
     * Delete existing data with id = 1 to insert our own.
     */
    private static void cleanupTables(Connection connection, boolean columnUserExist) throws SQLException {
        final Set<String> existingTables = new HashSet<>();

        ResultSet rs = connection.getMetaData().getTables(null, null, "%", null);
        while (rs.next()) {
            existingTables.add(rs.getString("TABLE_NAME"));
        }

        try (Statement statement = connection.createStatement()) {
            if (existingTables.contains("NOTEPAD")) {
                statement.executeUpdate("SET SQL_SAFE_UPDATES = 0");
                statement.executeUpdate("delete from NOTEPAD\n" +
                                                "where NOTEPAD.AD_ID IN (SELECT ID FROM AD WHERE ID IN (300001, 300002) OR CATEGORY_ID IN (200001, 200002, 9999999) OR USER_ID = 100001)\n" +
                                                "or NOTEPAD.USER_ID = 100001\n" +
                                                "or NOTEPAD.ID = 400001;");
            }

            if (existingTables.contains("AD")) {
                if (columnUserExist) {
                    statement.executeUpdate("delete from AD\n" +
                                                    "where AD.ID IN (300001, 300002)\n" +
                                                    "or AD.CATEGORY_ID IN (200001, 200002, 9999999)\n" +
                                                    "or AD.USER_ID = 100001;");
                } else {
                    statement.executeUpdate("delete from AD\n" +
                                                    "where AD.ID IN (300001, 300002)\n" +
                                                    "or AD.CATEGORY_ID IN (200001, 200002, 9999999);");
                }
            }

            if (existingTables.contains("USER")) {
                statement.executeUpdate("delete from USER\n" +
                                                "where USER.ID = 100001;");
            }

            if (existingTables.contains("CATEGORY")) {
                statement.executeUpdate("delete from CATEGORY\n" +
                                                "where CATEGORY.ID = 200001 or CATEGORY.ID = 200002;");
            }
        }
    }

    public static void deleteUserInDatabase(String email, Connection connection) throws SQLException {
        final String deleteUser = "DELETE FROM USER\n" +
                "WHERE EMAIL = \"" + email + "\"";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            statement.executeUpdate(deleteUser);
        }
    }

    public static void deleteCategoryInDatabase(String categoryName, Connection connection) throws SQLException {
        final String deleteCategory = "DELETE FROM CATEGORY\n" +
                "WHERE NAME = \"" + categoryName + "\"";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            statement.executeUpdate(deleteCategory);
        }
    }

    public enum FieldType {
        INTEGER(Types.INTEGER), ENUMERATION(Types.CHAR, Types.VARCHAR), VARCHAR(Types.VARCHAR), DECIMAL(
                Types.DECIMAL), TIMESTAMP(Types.TIMESTAMP);

        private final List<String> values;

        FieldType(Integer... values) {
            this.values = Stream.of(values).map(String::valueOf).collect(Collectors.toList());
        }
    }
}
